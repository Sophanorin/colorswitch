using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformRotateControl : MonoBehaviour
{
	public float speed = 1;
	bool toY = true;
	float currentPos;
	// Start is called before the first frame update
	void Start()
	{
		currentPos = transform.position.y;
	}

	// Update is called once per frame
	void Update()
	{
		float posY = transform.position.y;
		if (posY >= currentPos + 0.3)
			this.toY = false;
		if (posY <= currentPos -0.3)
			this.toY = true;

		if (toY)
		{
			transform.Translate(0 , speed * Time.deltaTime , 0);
		}
		else
		{
			transform.Translate(0 , -speed * Time.deltaTime , 0);
		}
	}
}
