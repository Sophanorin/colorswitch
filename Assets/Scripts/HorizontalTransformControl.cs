using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalTransformControl : MonoBehaviour
{
	public float speed = 5;
	public bool toLeftDirection = true;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		float positionX = transform.position.x;
		if (positionX <= -21)
		{
			this.toLeftDirection = false;
		}

		if (positionX >= -3)
		{
			this.toLeftDirection = true;
		}

		if (toLeftDirection)
		{
			transform.Translate(-speed * Time.deltaTime , 0 , 0);
		}
		else
		{
			transform.Translate(speed * Time.deltaTime , 0 , 0);
		}
	}
}
