﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallControl : MonoBehaviour
{
    public string colour;
    public float jumpForce = 7f;
    public Color bluesea;
    public Color pink;
    public Color yellow;
    public Color purple;
    public Text scoreboard;
    public static int score = 0;
    public GameObject colorWheel;
    public GameObject[] colorSwitchers;
    public GameObject colorSwitchersController;

    // Start is called before the first frame update
    void Start()
    {
        RandomColor();
        score = 0;
        scoreboard.text = score.ToString();
        GetComponent<Rigidbody2D>().gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<Rigidbody2D>().gravityScale = 2;
            GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "IncreaseScore")
        {
            score += 1;
            scoreboard.text = score.ToString();

            cloneColorSwitcher();

            Destroy(collision.gameObject);
        }
        if (collision.tag == "ColorWheel")
        {
            RandomColor();

            colorWheel = cloneColorWheel();

            return;
        }
		if (collision.tag != colour && collision.tag != "IncreaseScore" || collision.tag == "Obstacle")
		{
			SceneManager.LoadScene(2);
		}


	}

    void RandomColor()
    {
        int randomNumber = Random.Range(0, 4);

        switch (randomNumber)
        {
            case 0:
                colour = "Bluesea";
                GetComponent<SpriteRenderer>().color = bluesea;
                break;
            case 1:
                colour = "Yellow";
                GetComponent<SpriteRenderer>().color = yellow;
                break;
            case 2:
                colour = "Pink";
                GetComponent<SpriteRenderer>().color = pink;
                break;
            case 3:
                colour = "Purple";
                GetComponent<SpriteRenderer>().color = purple;   
                break;

        }
    }

    private GameObject cloneColorSwitcher()
    {
        GameObject colorSwitcher = colorSwitchers[Random.Range(0, colorSwitchers.Length)];
        GameObject newColorSwitcher = Instantiate(colorSwitcher, new Vector3(transform.position.x, transform.position.y + 10f, transform.position.z), transform.rotation);

        newColorSwitcher.SetActive(true);

        newColorSwitcher.transform.SetParent(colorSwitchersController.transform);

        return newColorSwitcher;
    }

    private GameObject cloneColorWheel()
    {
        GameObject previousColorWheel = colorWheel;
        GameObject newColorWheel = Instantiate(previousColorWheel, new Vector3(transform.position.x, transform.position.y + 10f, transform.position.z), transform.rotation);

        Destroy(previousColorWheel);

        return newColorWheel;
    }

}
